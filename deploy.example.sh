#!/bin/bash
source ~/.bashrc
DIR_NAME="doc"
ROOT_DIR="/home/predoc"
LOG_FILE="/home/predoc/log.txt"

echo "$DIR_NAME" $(date) >> "$LOG_FILE" 
rm -Rf "$ROOT_DIR/$DIR_NAME"
mkdir "$ROOT_DIR/$DIR_NAME"
cd "$ROOT_DIR/$DIR_NAME-src/"
for f in ./*; do
  echo "$f"
  if test -d "$ROOT_DIR/$DIR_NAME-src/$f"; then
    LANG=$(echo "$f" | sed "s/\.\///")
    cd "$ROOT_DIR/$DIR_NAME-src/$LANG" && mkdocs build --clean && cp -a "$ROOT_DIR/$DIR_NAME-src/$LANG/site/" "$ROOT_DIR/$DIR_NAME/$LANG"
  fi    
done;
