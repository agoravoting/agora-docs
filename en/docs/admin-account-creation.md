You can create a free admin account in:

[https://nvotes.com](https://nvotes.com/)

Once the form is sent you will receive an 8 characters alphanumeric code by SMS (the authentication code) which you need to safely preserve to access the application.

## Admin access to the application

Once you have your admin account, you can access the application here:

[https://go.nvotes.com/admin/login](https://go.nvotes.com/admin/login)

You will be required to introduce your email or mobile phone number and the authentication code.

<!--

This [video](https://docs.google.com/file/d/0B0YWdwxH6K5XdUs5MmNUMmdIbXM/edit) shows the process that a voter needs to follow to cast a ballot.
 -->
