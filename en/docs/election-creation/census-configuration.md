In the census configuration it is specified whether new voters can be added to the census during the voting period (open census) or not (closed census). You can also add extra fields to the census table.

## Closed census

In an election with closed census, the administrator, previously to the election beginning, will load a list of voters in one of two possible ways (see 'Census Data' section). With this configuration, users are not allowed to self-register themselves (there is no form by which a person can add himself to the census), but the administrator can remove and add people in section 'Census Data'.

## Open census

This option allows users to register themselves in the census by filling a form during the period the election is active. The census could start from zero when the election is started or the administrator can load an initial census before starting the election and allow new votters to register themselves to the census while the election is live. The election administrator will define what data to gather from the voters participating in the election.

## Census fields

You can add as many extra fields to the census as you need. There will always be at least one extra field, which will be 'email' or 'tlf', depending on the authentication method chosen in the previous section.

<!--Try it out! You can see an [example of a signup page that showcases all the possible field types you can use]

See annex [Common fields](https://docs.google.com/document/d/1vJoeMALMSA7wp5Kzo3zILtjbD9rSK82Yl6ALM0-xdKI/pub#h.u4w0u32o2mu2)

-->

Once the election has started a signup webpage will be created. The administrator can share it (via email, social networks, publishing in a webpage etc) inviting people to sign up and vote.

### Field name

Name of the extra field

### Help text

Help text or description of the field

### Type

Allowed types:

* email
* password
* text
* int
* bool
* captcha
* textarea
* key-value
* tel (telephone number)

### Regular expression

Regular expression used for validation

### Min/Max

If the field is a number, min and max configuration will limit the acceptable range of the valid numbers. If the field is a string-type, it will set the minimum and maximum number of characters.

### Required

If an extra field is required, all the voters of the census must define it.

### Unique

If active, all values of this extra field for an election census must be different.

### Private

If active, the contents of this field will only be displayed to admin users. For example, if an election has an extra field called "DNI", and this field is defined for a certain voter voter1, his DNI won't be sent to him when the voter logins but it will be available to the election administrators.

### Checked against census in authentication

This field will be required for authentication and it should match with the value stored in the census for a successful login.

### Checked against census in registration (only for pre-registration)

This option is only used for pre-registration purposes: the election admin will create a census list with pre-registered users and he will fill in this extra field. When users register themselves, they must fill this field too, and the registration will be successful if they match.

### Required on registration, but can be empty on pre-registration

This option is only used for pre-registration purposes: the election admin will create a census list with pre-registered users and he may leave this extra field empty. When users register themselves, they must fill in this field.









