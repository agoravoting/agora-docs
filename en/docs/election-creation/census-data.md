In an election, the administrator, before the election is started, can load a list of voters from a [CSV file](http://es.wikipedia.org/wiki/CSV) (with the fields specified for the census as described in the census configuration section of this guide, if they were added, and which must appear in the CSV file in the same order and separated by a semicolon). It is very important that the administrator checks that the 'email' or 'tlf' fields are correct (for example, that there are no spaces or forbidden characters, etc) to have the certainty that all voters receive their message inviting them to vote. You can use an online service like [Neverbounce](https://neverbounce.com).

Once the election has started, the administrator will be able to send a message (via email or SMS) to all voters inviting them to vote.

The election administrator can add the census while creating the election, and he will be able to edit it at every step of the election process.

## Census list

The census list displays the list of eligible voters after applying the selected filters. For example, there is a search field, to search for voters which fields contain certain words, and if you click on the columns you can also sort, and apply a number of other filters to the census.

### ID

Each eligible voter has a unique number id, which won't be repeated even between elections.

### Created date

The date when this voter was added to the census.

### Active?

When the voter is active, he/she will be able to vote.

### Voted?

Whether this particular voter has already voted or not

### email/tlf

The email or telephone number (depending on authentication method) of each voter.

### Extra fields

After the email/tlf, all the extra fields will be displayed in different columns.

## Search

In this view, there is a search field, which can be used to search for voters that have email/tlf or extra fields that contain certain words.

## Refresh

If you press the refresh button, the census list will be reloaded, applying the selected filters. This button won't normally be necessary to be used as the census is automatically refreshed whenever there is a change.

## Actions

On the left side of each eligible voter in the census list there is a checkbox that can be used to select specific voters. When you click on the action button on the top right, a submenu will be displayed showing a list of actions that can be applied to all of the selected voters.

### Add person

Use this option to add a single voter to the census.

### Add CSV to census...

Use this option to add multiple voters to the election census. It will show a modal dialog with a text area where you can enter the voters' information.

The format is CSV, one person per line, and using a semicolon ";" to split fields, in order. The first field is the authentication field (telephone number/email), and then the extra fields in the same order as in the census list.

### Export all census to CSV

This option will generate a CSV file with all the voters in the census for this election, including fields like 'active', 'email'/'tlf', 'voted', and the hexadecimal field 'voterid'.

### Select all people shown

This action will select all voters displayed with the actual census filter.

### Deselect all people shown

This action will deselect all currently selected voters.

### Activate n selected people

This action will set as active (ie, they can vote) all currently selected voters.

### Deactivate n selected people

This action will set as not active (ie, they can't vote) all currently selected voters.

### Remove n selected people

This action will remove all selected voters from the election census.

### Send auth codes to n selected people

This action will send a message with the authentication codes required for voting to all currently selected voters.