## Election title

{!embedded-docs/election/title.md!}

## Election description

{!embedded-docs/election/description.md!}

## Custom Logo

{!embedded-docs/election/logo-url.md!}

## Number of votes per voter

{!embedded-docs/election/num-revotes.md!}
