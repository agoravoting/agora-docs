# Using HTML in text fields

Some fields in nVotes allow HTML input. It is very important that the HTML is well formed. If you think some HTML is not being displayed properly in nVotes, please check if it works ok in an external HTML renderer (such as http://htmledit.squarefree.com/).

# Working with images
Some fields include images. In nVotes images are hosted externally. This means that you don't upload images to nVotes, but specify their URL instead. The URL must be a file URL (such as `https://upload.wikimedia.org/wikipedia/commons/2/22/The_Beatles_magical_mystery_tour.jpg` or `https://cdn.pixabay.com/photo/2014/10/30/08/49/the-beatles-509069_640.jpg`), not an image's web page (such as `https://commons.wikimedia.org/wiki/File%3AThe_Beatles_magical_mystery_tour.jpg` or `https://pixabay.com/photo-509069/`). If you have any kind of problem using images in nVotes, you can check if an image URL is correct by writing `<img src="IMAGE_FILE_URL">` in http://htmledit.squarefree.com/ and see if it renders correctly.

You must also make sure that the server will allow this type of image usage, as some of them stop serving images if it is requested a high number of times in a short period of time. Most of the times images will be hosted in your organization's web site. Talk to your webmaster to make sure there wont be any problem.

Be careful with the size of the images, as it can slow down the loading of the voting booth.
