Before voters can cast their vote, they must authenticate themselves in the [login webpage](https://go.nvotes.com/election/4000208/public/login) with their Authentication Code received by email or SMS.

## Email authentication

Users will receive an email with the Authentication Code that enables them to vote. This means voters will be required to have an email account. It is recommended to send an email to all the census some days before the election starts, announcing that on the date the election starts they will receive an email from nVotes with the authentication codes.

Both the body and subject of the authentication email are configurable. You can include any text you want and you can add a link to the voting booth inserting the code `__URL__` and the Authentication Code inserting `__CODE__`. If you want the link included in the SMS to open the login page and automatically include the Authentication Code use `__URL__//__CODE__`

<img alt=' src='/doc/en/images/authentication-codes-email.png' alt='Authentication codes email'>

## SMS authentication

Users will receive an SMS with the Authentication Code that enables them to vote. This means voters will be required to have a mobile phone.

The SMS text is configurable, within the limits of 140 characters, and it cannot contain accents or strange characters. You can include any text you want and you can add a link to the voting booth inserting the code `__URL__` and the Authentication Code inserting `__CODE__`. In the same fashion as for email authentication, you can use the `__URL__//__CODE__` formula to include a link in the SMS for easy/automatic logins.
