When the options inside a question are grouped into categories, randomize categories show those categories themselves random order.

**Notes and recomendations**

- This is an advanced feature. We do not recommend its use if you don't know what it does or that you need it.
- The randomization means that each time the voting booth is loaded, the categories are shown in a different order.
