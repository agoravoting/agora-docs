Election description is where you can explain valuable information to voters, the electorate and the public.

**Format**

- It is optional, you can leave it blank (although it is not recommended).
- It can contain up to 3,000 characters.
- It allows simplified HTML:
    - You can mark as **bold** something with `<strong>your text goes here</strong>`
    - You can mark as **italic** something with `<i>our text goes here</i>`
    - You can create a link like <a href="https://nvotes.com" target="_blank">this one</a> with `<a href="https://nvotes.com">this one</a>`. Links will always open in a new tab.
    - You can add paragraphs with two new line  HTML elements as in `<br><br>`. **Do not use the `<p>`** element because the whole description is inside a single `<p>` element.
    - Other HTML elements will be sanitized and are not allowed.

**Where it appears**

- It appears in the public page of the election.

  For example in <a href="https://go.nvotes.com/election/4000020/public/home" target="_blank">this election</a> the description is  *"Description of election"*. Appears underlined in the following image:

<div style="text-align: center;">
<img src="/doc/en/images/election_home_election_description.png" alt="Election description in the public page of the election" />
</div>

- It appears in the start screen of the voting booth.

  For example in <a href="https://go.nvotes.com/booth/4000020/vote" target="_blank">this election</a> the description is  *"Description of the election"*. Appears underlined in the following image:

<div style="text-align: center;">
<img src="/doc/en/images/voting_booth_election_description.png" alt="Election description in the voting booth" />
</div>

- The election results (available in different formats, like the <a href="https://go.nvotes.com/election/4000132/public/home" target="_blank">public website</a>, the <a href="https://go.nvotes.com/elections/public//4000132/results-fdadc293-0253-4e0d-9d64-5d68d85718c7/4000132.results.pdf" target="_blank">PDF</a>, <a href="https://go.nvotes.com/elections/public//4000132/results-fdadc293-0253-4e0d-9d64-5d68d85718c7/4000132.results.pdf" target="_blank">CSV</a> and other formats of the election results files available through the election dashboard.

**Notes and recomendations**

- Links can be helpful to ease access to a webpage with information on candidates, a document, the organization statutes, etc... or a link to a video explaining the voting process (for example, a video recorded with a smarthphone, camtasia, screenr, screencastle, etc… in a test election).
- If the text is very large, it will appear partially collapsed in the voting booth with a small icon to fully expand the description.
- It is not advisable to use a very large description, because many voters won't read it.
