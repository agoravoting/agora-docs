Configures how many options wins for the question.

**Format**

- It is always a non-zero positive integer.
- It cannot be greater than the number of options of the question.

**Where it appears**

- It appears in the public page of the election.

  For example in <a href="https://go.nvotes.com/election/4000021/public/home" target="_blank">this election</a> the number of winners for the first and only question is one. Appears underlined in the following image:

<div style="text-align: center;">
<img src="/doc/en/images/election_home_num_winners.png" alt="Number of winners in the public page of an election" />
</div>

- The election results (available in different formats, like the <a href="https://go.nvotes.com/election/4000132/public/home" target="_blank">public website</a>, the <a href="https://go.nvotes.com/elections/public//4000132/results-fdadc293-0253-4e0d-9d64-5d68d85718c7/4000132.results.pdf" target="_blank">PDF</a>, <a href="https://go.nvotes.com/elections/public//4000132/results-fdadc293-0253-4e0d-9d64-5d68d85718c7/4000132.results.pdf" target="_blank">CSV</a> and other formats of the election results files available through the election dashboard.
