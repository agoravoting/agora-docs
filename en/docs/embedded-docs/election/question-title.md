The title of this question, for example *"Who do you want to be the president of the Association?"*.

**Format**

- Only plain text is allowed. Does not allow any HTML or other kind of formatting.
- It is a required field, must not be empty and can contain a maximum of 300 characters.
- UTF-8 characters are allowed, but be aware that some characters might not be supported by the font types where the title appears.
- No new lines are not supported.

**Where it appears**

- The public pages of the election.

  For example in <a href="https://go.nvotes.com/election/4000020/public/home" target="_blank">this election</a> an election with the first question titled *"Should we use Electronic Elections?"*. Appears underlined in the following image:

<div style="text-align: center;">
<img src="https://nvotes.com/predoc-print/en/images/election_home_question_title.png" alt="Question title in the public page of the election" />
</div>

- Inside the voting booth when the user answers the question.

  For example in <a href="https://go.nvotes.com/booth/4000020/vote" target="_blank">this voting booth</a> the first question is titled *"Should we use Electronic Elections?"*. Appears underlined in the following image:

<div style="text-align: center;">
<img src="https://nvotes.com/predoc-print/en/images/voting_booth_question_title.png" alt="Question title in the voting booth" />
</div>

- The election results (available in different formats, like the <a href="https://go.nvotes.com/election/4000132/public/home" target="_blank">public website</a>, the <a href="https://go.nvotes.com/elections/public//4000132/results-fdadc293-0253-4e0d-9d64-5d68d85718c7/4000132.results.pdf" target="_blank">PDF</a>, <a href="https://go.nvotes.com/elections/public//4000132/results-fdadc293-0253-4e0d-9d64-5d68d85718c7/4000132.results.pdf" target="_blank">CSV</a> and other formats of the election results files available through the election dashboard.

**Notes and recomendations**

- Because of its prominence, the **question title should be short**, descriptive and to the point.
- **Don't confuse the question title with election title**: a single election can contain multiple questions.
