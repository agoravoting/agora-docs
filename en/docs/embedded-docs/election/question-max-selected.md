The maximum number of options that a voter can select in a specific question in the voting booth.

**Format**

- It is always a positive non-zero integer.
- It cannot be greater than the maximum number of options available.

**Where it appears**

- It appears when the question is shown in the voting booth.

  For example in <a href="https://go.nvotes.com/booth/4000020/vote" target="_blank">this election</a> the maximum number of options that can be selected in the first question is *1*. Appears underlined in the following image:

<div style="text-align: center;">
<img src="/doc/en/images/voting_booth_question_max.png" alt="Question maximum number of selected options in the voting booth" />
</div>

- It appears in the public page of the election.

  For example in <a href="https://go.nvotes.com/election/4000021/public/home" target="_blank">this election</a> the maximum number of options that a voter can select  for the first and only question is one. Appears underlined in the following image:

<div style="text-align: center;">
<img src="/doc/en/images/election_home_question_max.png" alt="Maximum number of selected options in the public page of an election" />
</div>

- The election results (available in different formats, like the <a href="https://go.nvotes.com/election/4000132/public/home" target="_blank">public website</a>, the <a href="https://go.nvotes.com/elections/public//4000132/results-fdadc293-0253-4e0d-9d64-5d68d85718c7/4000132.results.pdf" target="_blank">PDF</a>, <a href="https://go.nvotes.com/elections/public//4000132/results-fdadc293-0253-4e0d-9d64-5d68d85718c7/4000132.results.pdf" target="_blank">CSV</a> and other formats of the election results files available through the election dashboard.

**Notes and recomendations**

- In the voting Borda Traditional, the points that are given to each option depend on this setting: the first selected option obtains a number of points equal to the maximum number of selected options, the second selected option obtains number of points equal to the maximum number of selected options minus 1, etc.
