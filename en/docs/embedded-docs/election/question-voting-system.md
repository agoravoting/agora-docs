A question's voting system is the set of rules that determines how ballots are marked, cast and counted.

nVotes supports the following voting systems:

## Plurality at large

### How it works

- Ballot:
    - Each voter can mark one or more options without order
- Tally:
    - Each option obtains a number of points equal to the number of ballots in which they are marked.
    - One or more winners are chosen from those options with more points.

### Example

Question: What option do you prefer? (choose 0 to 2 options, 1 winner)

    Voters          |                                  Tally
    ------------------------------------------------------------------------------------------------------
    Ballot 1:       |      Ballot 1:            |
    -------------   |      -------------        |
    [X] Option A    |      Option A -> 1 points |
    [ ] Option B    |      Option B -> 0 points |
    [ ] Option C    |      Option C -> 0 points |     Option A: 2 points  |
                    |-->                        |---> Option B: 0 points  |--> Winner: Option A (2 points)
    Ballot 2:       |      Ballot 2:            |     Option C: 1 point   |
    -------------   |      -------------        |
    [X] Option A    |      Option A -> 1 points |
    [ ] Option B    |      Option B -> 0 points |
    [X] Option C    |      Option C -> 1 points |

### Notes

This is the most basic and typical voting system and the simplest to understand. Recommended for:

- Yes/No/Abstain questions
- Simple single winner questions

## Nauru's Borda Count or Borda Dowdall (1/n)

### How it works

- Ballot:
    - Each voter can choose in order one or more (number N) options
- Tally:
    - The first option chosen in a ballot obtains 1 points, the next option chosen obtains 1/2 points, the next option chosen obtains 1/3 points, etc
    - One or more winners are chosen from those options with more points.

### Example

Question: What option do you prefer? (choose 0 to 2 options, 1 winner)

    Voters          |                                  Tally
    ------------------------------------------------------------------------------------------------------
    Ballot 1:       |      Ballot 1:              |
    -------------   |      -------------          |
    [1] Option A    |      Option A -> 1 points   |
    [ ] Option B    |      Option B -> 0 points   |
    [ ] Option C    |      Option C -> 0 points   |     Option A: 2 points   |
                    |-->                          |---> Option B: 0 points   |--> Winner: Option A (2 points)
    Ballot 2:       |      Ballot 2:              |     Option C: 0.5 points |
    -------------   |      -------------          |
    [1] Option A    |      Option A -> 1 points   |
    [ ] Option B    |      Option B -> 0 points   |
    [2] Option C    |      Option C -> 0.5 points |


## Borda Count (traditional)

### How it works

- Ballot:
    - Each voter can choose in order one or more (number N) options
- Tally:
    - The first option chosen in a ballot obtains N points, the next option chosen obtains N-1 points, the next option chosen obtains N-2 points, etc
    - One or more winners are chosen from those options with more points.

### Example

Question: What option do you prefer? (choose 0 to 2 options, 1 winner)

    Voters          |                                  Tally
    ------------------------------------------------------------------------------------------------------
    Ballot 1:       |      Ballot 1:            |
    -------------   |      -------------        |
    [1] Option A    |      Option A -> 2 points |
    [ ] Option B    |      Option B -> 0 points |
    [ ] Option C    |      Option C -> 0 points |     Option A: 4 points  |
                    |-->                        |---> Option B: 0 points  |--> Winner: Option A (4 points)
    Ballot 2:       |      Ballot 2:            |     Option C: 1 point   |
    -------------   |      -------------        |
    [1] Option A    |      Option A -> 2 points |
    [ ] Option B    |      Option B -> 0 points |
    [2] Option C    |      Option C -> 1 points |

