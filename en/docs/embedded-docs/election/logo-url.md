URL corresponding to a logo that will be displayed in various places to make the election more adapted to the image of your organization.

**Plans**

This feature is not available in the Free plan, only available in the Prime and Prime Plus plans. Please check our <a href="https://nvotes.com/pricing" target="_blank">pricing plans</a> for more information.

**Format**

- It is an optional field and is empty by default. When left empty, it will be replaced with <a href="https://nvotes.com" target="_blank">nVotes logo</a>.
- The **URL must start with `https://` and not `http://`** because otherwise web browsers won't show the image for security reasons.
- The URL of the logo cannot be longer than 300 characters.
- An example valid URL could be: `/doc/en/images/nvotes_online_voting_logo_blue_long_for_web.png`

**Where it appears**

- It appears in the top left corner of public page of the election.

  You can see it for example in <a href="https://go.nvotes.com/election/4000025/public/home" target="_blank">this election</a>. Appears underlined in red in the following image:

<div style="text-align: center;">
<img src="/doc/en/images/election_home_logo.png" alt="Election logo in the public page of the election" />
</div>

- It appears in the top left corner of the voting booth.

    You can see it for example in <a href="https://go.nvotes.com/booth/4000025/vote" target="_blank">this election</a>.. Appears underlined in red in the following image:

<div style="text-align: center;">
<img src="/doc/en/images/voting_booth_logo.png" alt="Election logo in the voting booth" />
</div>

**Notes and recomendations**

- The logo will not be shrinked, it will be shown at the size you provided.
- The recommended format of the logo image should be PNG or JPG, but any image format allowed by a web browser will be valid.
- Because the logo appears in top navegation bar, it is recommended to use a logo with a height of around 30 pixels, which is the default size of the top bar plus 10 pixels of separation from the border. Because this a small image, you can remove any border in your logo image to make it appear bigger.
- The maximum recommended size is 150x150 pixels.
