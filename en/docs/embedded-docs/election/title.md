The title of the election, for example "ABC Association General Meeting 2019".

**Format**

- Only plain text is allowed. Does not allow any HTML or other kind of formatting.
- It is a required field, must not be empty and can contain a maximum of 300 characters.
- UTF-8 characters are allowed, but be aware that some characters might not be supported by the font types where the title appears.
- The new line character is not supported.

**Where it appears**

- The public pages of the election.

  For example in <a href="https://go.nvotes.com/election/4000020/public/home" target="_blank">this election</a> an election the title is  *"Sample election"*. Appears underlined in the following image:

<div style="text-align: center;">
<img src="/doc/en/images/election_home_election_title.png" alt="Election title in the public page of the election" />
</div>

- The initial screen of the voting booth.

  For example in <a href="https://go.nvotes.com/booth/4000020/vote" target="_blank">this election</a> an election the title is  *"Sample election"*. Appears underlined in the following image:

<div style="text-align: center;">
<img src="/doc/en/images/voting_booth_election_title.png" alt="Election title in the voting booth" />
</div>

- The election results, that are available in different formats like the <a href="https://go.nvotes.com/election/4000132/public/home" target="_blank">public website</a>, the <a href="https://go.nvotes.com/elections/public//4000132/results-fdadc293-0253-4e0d-9d64-5d68d85718c7/4000132.results.pdf" target="_blank">PDF</a>, <a href="https://go.nvotes.com/elections/public//4000132/results-fdadc293-0253-4e0d-9d64-5d68d85718c7/4000132.results.pdf" target="_blank">CSV</a> and other formats of the election results files available through the election dashboard.

**Notes and recomendations**

- Because of its prominence, the election title should be descriptive, but short and to the point.
- Don't confuse the election title with question title: a single election can contain multiple questions.
