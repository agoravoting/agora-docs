The minimum number of options that a voter can select in a specific question in the voting booth.

**Format**

- It is always a positive integer (zero or more).
- It cannot be greater than the maximum number of options that a voter can select.

**Where it appears**

- It appears when the question is shown in the voting booth.

  For example in <a href="https://go.nvotes.com/booth/4000020/vote" target="_blank">this election</a> the minimum number of options that can be selected in the first question is *0*. Appears underlined in the following image:

<div style="text-align: center;">
<img src="/doc/en/images/voting_booth_question_min.png" alt="Question minimum number of selected options in the voting booth" />
</div>

- It appears in the public page of the election.

  For example in <a href="https://go.nvotes.com/election/4000021/public/home" target="_blank">this election</a> the minimum number of options that a voter can select  for the first and only question is one. Appears underlined in the following image:

<div style="text-align: center;">
<img src="/doc/en/images/election_home_question_min.png" alt="Minimum number of selected options in the public page of an election" />
</div>

- The election results (available in different formats, like the <a href="https://go.nvotes.com/election/4000132/public/home" target="_blank">public website</a>, the <a href="https://go.nvotes.com/elections/public//4000132/results-fdadc293-0253-4e0d-9d64-5d68d85718c7/4000132.results.pdf" target="_blank">PDF</a>, <a href="https://go.nvotes.com/elections/public//4000132/results-fdadc293-0253-4e0d-9d64-5d68d85718c7/4000132.results.pdf" target="_blank">CSV</a> and other formats of the election results files available through the election dashboard.

**Notes and recomendations**

- If you want to allow voters to vote in blank, you can set the minimum number of selected options to zero.
- If the minimum number of selected options is set to zero and a voter doesn't select any option in the question, a warning will be shown to confirm the voter wants to vote in blank.
