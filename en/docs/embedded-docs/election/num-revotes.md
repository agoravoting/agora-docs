Number of times a voter can vote and change his vote by voting again during the voting period.

**Format**

- Allowed values are integer numbers between 0 and 255.
- 0 (the default) means disabling this limit and means that voter has no specific limit on the number of times he can change his vote.
- 1 would mean that a voter can only vote once and can't change his vote, 2 would mean that a voter can change his vote but only once and so on.

**Notes and recomendations**

- When a voter votes again, the previous cast vote is discarded and only his most recent vote counts. This means that the principle one-voter-one-vote is always preserved.
- Allowing a voter to change his vote like **this is a security feature** to mitigate against coercion. By allowing voters to change their votes a voter is coerced to vote in some specific manner, the voter can later on, when he is not coerced, change his vote to reflect his genuine preferences.
- Even if this limit is disabled by setting the value to zero, the server side of the ballot box implements additional limits on the number of revotes to mitigate Denial of Service (DoS) attacks by voters.
