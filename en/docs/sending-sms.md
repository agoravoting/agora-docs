SMS messages are sent using a provider with ISO27001 certification.


## Does it really cost the same to send SMS to any country?

Yes, whatever is the country you are sending the SMS to, the price is the same.

## Will my messages be sent quickly?

Your messages will arrive immediately. Our SMS provider is commited to process 90% of the messages in less than 5 seconds and 95% of them in less than 20 seconds.

## Which countries can I send SMS to with this platform?

Our SMS provider has connections with more than 150 countries.

<!-- list from https://support.twilio.com/hc/en-us/articles/223133767-International-support-for-Alphanumeric-Sender-ID  -->
These are the supported countries (this list may change in the future):

* Afghanistan
* Albania
* Algeria
* Andorra
* Angola
* Anguilla
* Antigua & Barbuda
* Argentina
* Armenia
* Aruba
* Australia
* Austria
* Azerbaijan
* Bahamas
* Bahrain
* Bangladesh
* Barbados
* Belarus
* Belgium
* Belize
* Benin
* Bermuda
* Bhutan
* Bolivia
* Bosnia & Herzegovina
* Botswana
* Brazil
* Brunei Darussalam
* Bulgaria
* Burkina Faso
* Burundi
* Cambodia
* Cameroon
* Canada
* Cape Verde
* Cayman Islands
* Central African Republic
* Chad
* Chile
* China*
* Colombia
* Comoros
* Congo
* Congo D.R.
* Cook Islands
* Costa Rica
* Croatia
* Cuba
* Cyprus
* Czech Republic
* Denmark
* Diego Garcia
* Djibouti
* Dominica
* Dominican Republic
* Ecuador
* Egypt
* El Salvador
* Equatorial Guinea
* Estonia
* Falkland Islands
* Faroe Islands
* Fiji
* Finland
* France
* French Guiana
* French Polynesia
* Gabon
* Gambia
* Georgia
* Germany
* Ghana
* Gibraltar
* Greece
* Greenland
* Grenada
* Guadeloupe
* Guam
* Guatemala
* Guernsey
* Guinea
* Guinea-Bissau
* Guyana
* Haiti
* Honduras
* Hong Kong
* Hungary
* Iceland
* India
* Indonesia
* Iran
* Iraq
* Ireland
* Isle of Man
* Israel
* Italy
* Cote dIvoire
* Jamaica
* Japan
* Jersey
* Jordan
* Kazakhstan
* Kenya
* Kuwait
* Kyrgyzstan
* Laos PDR
* Latvia
* Lebanon
* Lesotho
* Liberia
* Libya
* Liechtenstein
* Lithuania
* Luxembourg
* Macau
* Macedonia
* Madagascar
* Malawi
* Malaysia
* Maldives
* Mali
* Malta
* Martinique
* Mauritania
* Mauritius
* Mayotte
* Mexico
* Moldova
* Monaco
* Mongolia
* Montenegro
* Montserrat
* Morocco
* Mozambique
* Myanmar
* Namibia
* Nauru
* Nepal
* Netherlands
* Netherlands Antilles
* New Caledonia
* New Zealand
* Nicaragua
* Niger
* Nigeria
* Norfolk Island
* Norway
* Oman
* Pakistan
* Palestinian Territory
* Panama
* Papua New Guinea
* Paraguay
* Peru
* Philippines
* Poland
* Portugal
* Puerto Rico
* Qatar
* Reunion
* Romania
* Russia
* Rwanda
* Samoa
* San Marino
* Sao Tome & Principe
* Saudi Arabia
* Senegal
* Serbia
* Seychelles
* Sierra Leone
* Singapore
* Slovakia
* Slovenia
* Solomon Islands
* Somalia
* South Africa
* South Korea
* South Sudan
* Spain
* Sri Lanka
* St Kitts & Nevis
* St Lucia
* St Vincent & the Grenadines
* Sudan
* Suriname
* Swaziland
* Sweden
* Switzerland
* Syria
* Taiwan
* Tajikistan
* Tanzania
* Thailand
* Timor-Leste
* Togo
* Tonga
* Trinidad & Tobago
* Tunisia
* Turkey
* Turkmenistan
* Turks & Caicos Islands
* Uganda
* Ukraine
* United Arab Emirates
* United Kingdom
* United States
* Uruguay
* Uzbekistan
* Vanuatu
* Venezuela
* Vietnam
* Virgin Islands British
* Yemen
* Zambia
* Zimbabwe

<!--

## How much will it cost me to send texts messages?

The cost per message is 20 cents (0.20 €).

-->
