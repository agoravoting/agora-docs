# Introduction

This private repository contains the official documentation for nVotes (https://nvotes.com), an online software as a service product from Agora Voting.

# Installation

The documentation uses mkdocs. You can fine a more detailed description of how to install this locally in your machine in http://www.mkdocs.org/ . It's recommended for developers (but not mandatory) to install in their local machines this repository to be able to see the result of their changes locally without having to create and push a commit.

The procedure for installation is simple:

In order to install MkDocs you'll need Python installed on your system, as well as the Python package manager, pip. You can check if you have these already installed like so:

    $ python --version
    Python 2.7.2
    $ pip --version
    pip 1.5.2

MkDocs supports Python 2.6, 2.7, 3.3, 3.4 and 3.5.

On Windows we recommend that you install Python and pip with Chocolatey.

We like to use virtualenv to install dependencies. Please make sure python-virtualenv is installed if you are going to use it. Then create the virtualenv environment:

    $ mkvirtualenv agora-docs

Now download agora-docs repository. Note that it's a private gitlab repository, so please check you have permission first, and also check that git is installed in your system:

    $ git clone git@gitlab.com:agoravoting/agora-docs.git

And install the dependencies:

    $ cd agora-docs/
    agora-docs $ pip install -r requirements.txt

Finally, you can launch your local deployment:

    agora-docs $ mkdocs serve
    INFO    -  Building documentation...
    INFO    -  Cleaning site directory
    [I 160229 17:14:07 server:281] Serving on http://127.0.0.1:8000
    [I 160229 17:14:07 handlers:59] Start watching changes
    [I 160229 17:14:07 handlers:61] Start detecting changes

# Deployment

Currently the documentation is being automatically deployed in a pre-production environment which can be seen here: https://nvotes.com/predoc/ .

Comitting to the master branch will automatically trigger an update of the pre environment. This process usually takes a few seconds.

The pre environment deployment is managed by votular-web-prod AWS machine, through a gitautodeploy system service that works using the follow software: https://github.com/olipo186/Git-Auto-Deploy

In the inminent future, the documentation will probably launched in a production environment in https://nvotes.com/doc/ When that happens, we will have two environment (pre and pro), and each environment will track a different branch. Changes will usually happen first in "next" branch, and then merged through a pull request into "master" branch.